const { Pool } = require('pg');
const bluebird = require('bluebird');

const db = new Pool({
  poolSize: 20,
  user: 'tourism',
  password: 'tourism',
  database: 'tourism',
  Promise: bluebird
});

module.exports = db;
